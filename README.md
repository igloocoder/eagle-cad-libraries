#Alps
* [EC11EBB24C03 Dual Shaft Encoder](http://www.newark.com/alps/ec11ebb24c03/encoder-dual-shaft-11mm/dp/75T6668?ost=EC11EBB24C03 "")

#FCI

#Japan Solderless Terminals
* [JST B3B-PH-K-S(LF)(SN) Wire-Board 3-Pin Header Connector - 2mm](http://www.newark.com/jst-japan-solderless-terminals/b3b-ph-k-s-lf-sn/wire-board-connector-header-3/dp/37K9946?ost=B3B-PH-K-S%28LF%29%28SN%29 "")
* [ST B3B-PH-K-S(LF)(SN) Wire-Board 4-Pin Header Connector - 2mm](http://www.newark.com/jst-japan-solderless-terminals/b4b-ph-k-s-lf-sn/wire-to-board-connector-header/dp/37K9958?ost=B4B-PH-K-S%28LF%29%28SN%29 "")
* [JST PHR-5 Wire-Board 5-Pin Connector](http://www.newark.com/jst-japan-solderless-terminals/phr-5/wire-to-board-connector-housing/dp/92K8051?searchRef=SearchLookAhead "")

#Lorlin
* [Lorlin CK1027 Rotary Switch](http://www.newark.com/lorlin/ck1027/rotary-switch-3-30-150ma-250v/dp/25M8649?ost=ck1027 "")

#Multicomp
* [Multicomp 2214S-16SG-28 Board-Board 16 Position Socket Connector](http://www.newark.com/multicomp/2214s-16sg-85/board-board-connector-socket-16/dp/08N6809?ost=2214S-16SG-85 "")
* [Multicomp SPC21193 Pushbutton SPST Switch](http://www.newark.com/multicomp/spc21193/switch-pushbutton-spst-3a-250v/dp/11M0387?ost=SPC21193 "")
* [Multicomp SPC21360 RCA/Phono Connector Jack](http://www.newark.com/multicomp/spc21360/connector-rca-phono-jack/dp/11M0555?ost=SPC21360 "")

#Vishay
* [Vishay TDSL1160 Untinted Red 7-Segment LED Common Cathode Display](http://www.newark.com/vishay-semiconductor/tdsl1160/display-seven-segment-7mm-red/dp/33C1202?ost=TDSL1160 "")
